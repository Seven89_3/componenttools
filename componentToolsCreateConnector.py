from maya.api import OpenMaya as om2
from maya import cmds

from componentTools import componentToolsGuideConnector


class ComponentToolsCreateConnector(object):
    def __init__(self):
        self.connector_guide = componentToolsGuideConnector.ComponentToolsGuideConnector()

    def mob_from_active_selection(self):
        """
        Gives a list of the active Maya selection
        :return `MObject`
        """
        sel_list = []
        sel = om2.MGlobal.getActiveSelectionList()
        selLength = sel.length()

        for i in xrange(selLength):
            eachMob = sel.getDependNode(i)
            sel_list.append(eachMob)

        return sel_list

    def create_component_hierarchy(self, component_name):

        is_done = False
        component = cmds.createNode("transform", name="{}_component".format (component_name))

        # Create Input
        input = cmds.createNode("transform", name="{}_input".format(component_name))
        cmds.parent(input, component)

        # Create Output
        output = cmds.createNode("transform", name="{}_output".format(component_name))
        cmds.addAttr(output, ln="isAffected", at="bool")
        cmds.addAttr(output, ln="outChainToBind", at="matrix", multi=True)
        cmds.parent(output, component)

        # Create Deform
        deform = cmds.createNode("transform", name="{}_deform".format(component_name))
        cmds.parent(deform, component)

        # Create control
        control = cmds.createNode("transform", name="{}_control".format(component_name))
        cmds.parent(control, component)

        # Create Guide hrc
        guide = cmds.createNode("transform", name="{}_guide".format(component_name))
        guide_under_grps_suffix = ["_guide_parameter", "_guide_control", "_guide_tool"]

        for each in guide_under_grps_suffix:
            guide_under_grp = cmds.createNode("transform", name="{0}{1}".format(component_name, each))
            cmds.parent(guide_under_grp, guide)

            if each =="guide_tool":
                cmds.addAttr(guide_under_grp, ln="isGuided", at="bool")
                cmds.addAttr(guide_under_grp, ln="showGuides", at="bool")
                cmds.addAttr(guide_under_grp, ln="negateComponent", at="bool")

                cmds.addAttr(guide_under_grp, ln="toDelete", at="message", m=True)

                cmds.addAttr(guide_under_grp, ln="toSwap", at="compound", m=True, numberOfChildren=2)
                cmds.addAttr(guide_under_grp, ln="origin", at="message", p="toSwap")
                cmds.addAttr(guide_under_grp, ln="guided", at="message", p="toSwap")

        cmds.parent(guide, component)

        is_done = True
        return is_done

    def get_out_chain_elements(self, output, string=False):

        plug_list = []
        mfn_output = output

        if self.connector_guide.check_plug_exist(mfn_output, "outChainToBind"):
            plug_out_chain = mfn_output.findPlug("outChainToBind", False)
            elems_out_chain = plug_out_chain.evaluateNumElements()

            for i in xrange(elems_out_chain):

                plug = plug_out_chain.elementByPhysicalIndex(i)
                print plug.source()
                if plug.source():

                    if string:
                        plug = plug.name()
                        plug_list.append(plug)
                    else:
                        plug_list.append(plug)
                else:
                    continue

            return plug_list

    def connect_affection(self, plug_source, target):
        """
        Connects the
        :param plug_source: `str` plug of the output from the component which are affecting the target
        :param target: `str` container name
        :return boolean: True if succesfull
        """
        name_target_container = target
        mob_target_container = self.connector_guide.create_mob_from_node(name_target_container)
        mfn_target_container = om2.MFnDependencyNode(mob_target_container)
        target_affectedBy_exist = self.connector_guide.check_plug_exist(mfn_target_container, plug="affectedBy")

        plug_source_output = plug_source
        name_source_output = plug_source.split(".")[0]
        mob_source_output = self.connector_guide.create_mob_from_node(name_source_output)
        mob_source_container = self.connector_guide.container_from_node(mob_source_output)
        mfn_source_container = om2.MFnDependencyNode(mob_source_container)
        name_source_container = mfn_source_container.name()
        source_affect_exist = self.connector_guide.check_plug_exist(mfn_source_container, plug="affect")

        if source_affect_exist:
            plug_source_affect = mfn_source_container.findPlug("affect", False)
            plug_affect_elements = plug_source_affect.numElements()
            plug_affect_elem_connect = plug_affect_elements+1 # Adds 1 to get one step further in the array from currently used plugs

            if target_affectedBy_exist:
                plug_target_affectedBy = mfn_target_container.findPlug("affectedBy", False)

                cmds.connectAttr(plug_source_output, "{}[{}]".format (plug_source_affect, plug_affect_elem_connect))
                cmds.connectAttr("{}[{}]".format (plug_source_affect, plug_affect_elem_connect), plug_target_affectedBy)

            else:
                print "{}: Does not have an .affectedBy attribute".format(name_target_container)

        else:
            print "{}: Does not have an .affect attribute".format (name_source_container)

    def connect_dec_mtx_to_joint(self, dec_mtx, joint):

        cmds.connectAttr("{}.outputTranslate".format(dec_mtx), "{}.translate".format(joint))
        cmds.connectAttr("{}.outputRotate".format(dec_mtx), "{}.rotate".format(joint))
        cmds.connectAttr("{}.outputScale".format(dec_mtx), "{}.scale".format(joint))

    def build_binding(self, top_container):
        """ Create the joints used for binding from all component containers connected to the
            affects-> affectedBy attribute of the same.
        :param top_container: `MObjecthandle` An object of an container node of the top/master a component.
        :return boolean: True if created succesfully
        """
        component_containers_hierarchy = [top_container]

        for container in component_containers_hierarchy:
            if container.isValid():
                mob_container = container.object()
                mfn_container = om2.MFnDependencyNode(mob_container)
                mfn_output = self.connector_guide.get_mfn_from_component(mfn_container.name(), "output")[0]
                mob_output_name = self.connector_guide.create_mob_from_node(mfn_output.fullPathName())
                component_name = mfn_container.name().replace("_container", "")

                if self.connector_guide.check_plug_exist(mfn_output, "outChainToBind"):
                    plug_out_chain = mfn_output.findPlug("outChainToBind", False)
                    elems_out_chain = plug_out_chain.evaluateNumElements()

                    for i in xrange(elems_out_chain):
                        out_elem_pad = "{:03}".format(i)
                        plug_out_chain_elem = plug_out_chain.elementByPhysicalIndex(i)
                        #source_node_name = plug.source().name().split(".")[0] # Ger inget sakert namn
                        #source_useful_name = source_node_name.split("_")[0:3]
                        # The first item in the array has to be multiplied with its parent component, if top component, none.
                        if i == 0:
                            if self.connector_guide.check_plug_exist(mfn_container, "affectedBy"):
                                plug_affected_by = mfn_container.findPlug("affectedBy", False)
                                source_container_affected_by = plug_affected_by.source()
                                source_affect = source_container_affected_by.source()
                                if source_affect.isNull:
                                    # This is for the root joint of the binding
                                    dec_mtx_node = cmds.createNode("decomposeMatrix", name="{0}_{1}_decMtx".format (component_name, out_elem_pad))
                                    joint = cmds.createNode("joint", name="{0}_bind".format (component_name))
                                    cmds.connectAttr(plug_out_chain_elem, "{}.inputMatrix".format (dec_mtx_node))
                                    self.connect_dec_mtx_to_joint(dec_mtx_node, joint)

                                else:
                                    source_parent_out = source_affect.destinations()
                                    elems_source_parent_out = len(source_parent_out)
                                    # This can only be one item
                                    for j in range(elems_source_parent_out):
                                        plug = source_parent_out[j]
                                        # for the first item, take the last item in "outchain" from the ".affectedBy" component,
                                        # create a multMatrix and connect the .inverseWorldMatrix to [0] and worldMatrix of the first it the relevant array.
                                        # connect source_affect 
                                        dec_mtx_node = cmds.createNode("decomposeMatrix", name="{0}_{1}_decMtx".format(component_name, out_elem_pad))
                                        # Then parent it under the joint from the affectedBy joint
                            else:
                                print " doesnt have an affectedBy attribute"
                        else:
                            print "Hej"
                            # for the rest in the array, just decompose the local matrix and connect to the joint, then parent under the previous
                            #dec_mtx = cmds.createNode("decomposeMatrix", n="{}_bind".format (source_node_name))
                            pass

                if self.connector_guide.check_plug_exist(mfn_container, "affect"):
                    print "hejsan svejsan"
                    # Add the components which are connected to the ".affects" attribute of the current comntainer
                    plug_affects = mfn_output.findPlug("affect", False)
                    plug_affects_elements = plug_affects.evaluateNumElements()
                    for j in range(plug_affects_elements):
                        plug_sub_affects = plug_out_chain.elementByPhysicalIndex(j)
                        plugs_affected = plug_sub_affects.destinations()
                        print plugs_affected


            else:
                print "container mobha is not valid"


