from maya.api import OpenMaya as om2
from maya import cmds


class ComponentToolsGuideConnector(object):
    def __init__(self):
        self.CONTAINER_SUFFIX = "_container"
        self.TRACKER_PLUG_NAMES = ("origin", "guided")

    @staticmethod
    def create_mob_from_node(nodeName, mobha=False):
        selection = om2.MGlobal.getSelectionListByName(nodeName)
        mob = selection.getDependNode(0)
        return mob

    @staticmethod
    def check_plug_exist(object, plug=""):
        """
        Checks if the given plug exist on the given node
        :param object: `MFnDag`
        :return: `bool"
        """
        if plug is not None:
            try:
                plug = object.findPlug(plug, False)
                plug_exist = True
            except:
                plug_exist = False
        else:
            print (om2.MGlobal.displayError("No plug was passed as an argument"))

        return plug_exist

    @classmethod
    def get_containers_in_scene(self):
        """
        Iterating the scene for container nodes
        :return: `str` list
        """
        iter = om2.MItDependencyNodes(om2.MFn.kContainer)
        containers = []
        while not iter.isDone():
            mob = iter.thisNode()
            mfn = om2.MFnDependencyNode(mob)
            name = mfn.name()
            containers.append(name)
            iter.next()
        return containers

    @staticmethod
    def container_from_node(mayaNode):
        """
        Inspects a node connection set for standard containered topology and returns
          the owning container if one is found, None otherwise
        :param mayaNode: `MObject` any dependency node in Maya
        :return: `MObject | None` the container object the argument is linked to if there is one
                                    otherwise None
        """
        fnDep = om2.MFnDependencyNode(mayaNode)

        if mayaNode.hasFn(om2.MFn.kHyperLayout):
            container_name = fnDep.name()
            return container_name
        else:
            plug = fnDep.findPlug("message", False)

            for eachDestPlug in plug.destinations():
                destNode = eachDestPlug.node()
                if not destNode.hasFn(om2.MFn.kHyperLayout):
                    continue

                # at this point we"re dealing with an interesting node
                #  and we should find if it"s connected to a container
                fnDestNode = om2.MFnDependencyNode(destNode)
                layoutMsg = fnDestNode.findPlug("message", False)

                layoutDestinations = layoutMsg.destinations()
                for eachLayoutDestination in layoutDestinations:
                    if eachLayoutDestination.node().hasFn(om2.MFn.kContainer):
                        return eachLayoutDestination.node()

    def mob_to_name(self, mob):
        """
        This function takes a mob and returns the name as a string
        :param mob: `MObject` of a node
        :return: `str` of node name
        """
        name = None
        if mob.hasFn(om2.MFn.kDagNode):
            name = om2.MFnDagNode(mob).fullPathName()
        elif mob.hasFn(om2.MFn.kDependencyNode):
            name = om2.MFnDependencyNode(mob).name()
        return name

    def get_mfn_from_component(self, component_container, *args):
        """
        This function takes a components container node and returns the desired nodes from the "keywords" in args.
        :param component_container: `str` name of component container node
        :param *args: `str` for the components nodes you are after, any of "guide_tool, output, deform, control, guide" works
        :return: `list` MFnDagNodes
        """
        available_keynodes = ["guide_tool", "output", "deform", "control", "guide", "container"]

        component_mob = self.create_mob_from_node(component_container)
        component_name_and_mobha = self.create_mobha_from_object(component_mob)
        component_name = component_name_and_mobha[0]
        component_mobha = component_name_and_mobha[1]

        self.mfn_list = []

        for arg in args:
            if arg not in available_keynodes:
                om2.MGlobal.displayError("{}: is not a valid component object".format(i))
            elif arg == "container":
                mfn = om2.MFnDependencyNode(component_mob)
                self.mfn_list.append(mfn)
            else:
                mfn = self.get_component_object(component_name_and_mobha, objectType=arg)
                self.mfn_list.append(mfn)

        return self.mfn_list

    def get_top_component(self, containers_assorted=[]):

        for container in containers_assorted:
            mfn_container = self.get_mfn_from_component(container, "container")[0]
            plug_affected_by_exist = self.check_plug_exist(mfn_container, "affectedBy")
            if plug_affected_by_exist:
                plug_affected_by = mfn_container.findPlug("affectedBy", False)
                plug_source = plug_affected_by.source()
                if plug_source.isNull:
                    if "global" in container:
                        mob_container_top = self.create_mob_from_node(container)
                        break
            else:
                print "{}: Does not have the affect attribute".format(component)

        return om2.MObjectHandle(mob_container_top)

    def check_if_guided(self, guide_tool):
        """
        Checks if a component is in guided mode or not
        :param guide_tool: "MFnDagNode" The gui_tool node from the component
        :return: `bool`
        """
        is_guided = None
        if guide_tool is not None:
            plug_is_guided = guide_tool.findPlug("isGuided", False)

            if plug_is_guided.asBool():
                is_guided = True
            elif not plug_is_guided.asBool():
                is_guided = False

        return is_guided

    def create_mobha_from_object(self, mayaNode, CONTAINER_SUFFIX="_container"):
        """
        Generator iterating connected containers given an MObject iterator and an optional filter
        The filter will also act as a trim factor to reduce the name of the object,
          this helps since the name of the component shouldn"t include the suffix
          describing the object type inspected.
        :param mayaNode: `MFnDagNode`> A MObject of the container
        :param suffixFilter: `str` A string to restrict yield to items with a certain suffix.
                                   Can be None, False, or empty if filter is undesirable
        :return: tuple `(str, MObjectHandle)`: A tuple containing the name of the container and the
                                          Maya object handle of the container node
        """

        # container_name_and_mobha = ("", None)
        container = mayaNode
        if container is None:
            om2.MGlobal.displayError("{}.is not valid".format(container))

        k = om2.MFnDependencyNode(mayaNode).name()

        if CONTAINER_SUFFIX:
            if not k.endswith(CONTAINER_SUFFIX):
                om2.MGlobal.displayError("{0}:does not have the right suffix({1})".format(container, CONTAINER_SUFFIX))
            k = k[:-len(CONTAINER_SUFFIX)]
            mobha = om2.MObjectHandle(container)
        return k, mobha

    def iterate_mob_from_active_selection(self):
        """
        Generator iterating the active Maya selection
        :return: `MObject`
        """
        sel = om2.MGlobal.getActiveSelectionList()
        selLength = sel.length()

        for i in xrange(selLength):
            eachMob = sel.getDependNode(i)
            yield eachMob

    def key_objects_from_container(self, componentName, containerHandle):
        """
        This takes a component name for some filtering and a container handle and isolates
          the key objects related to it that represent our component interesting items.
        :param componentName: `str` mandatory now, used to compose full name of some exepcted items/paths
        :param containerHandle: `MObjectHandle`
        :return: `dict` k,v pairs for interesting objects and their handle, None if unavailable
        """

        mfn_cont = om2.MFnContainerNode(containerHandle.object())
        mobaMembers = mfn_cont.getMembers()

        keyObsDict = {
            "componentName": componentName,
            "control": None,
            "guide": None,
            "deform": None,
            "guide_tool": None,
        }

        for eachMob in mobaMembers:
            if not eachMob.hasFn(om2.MFn.kDagNode):
                continue

            mfn_dag = om2.MFnDagNode(eachMob)

            objectName = mfn_dag.name()

            if objectName == "{}_control".format(componentName):
                keyObsDict["control"] = om2.MObjectHandle(eachMob)
            elif objectName == "{}_guide".format(componentName):
                keyObsDict["guide"] = om2.MObjectHandle(eachMob)
            elif objectName == "{}_deform".format(componentName):
                keyObsDict["deform"] = om2.MObjectHandle(eachMob)
            elif objectName == "{}_output".format(componentName):
                keyObsDict["output"] = om2.MObjectHandle(eachMob)
            elif objectName == "{}_guide_tool".format(componentName):
                keyObsDict["guide_tool"] = om2.MObjectHandle(eachMob)

        return keyObsDict

    def tracker_subplugs_from_plug(self, plug):
        """
        This takes a well formed plug that is a compound and has two children and forms a dictionary from it
          containing those two sub plugs indexed by their expected names
        :param plug: `MPlug` Compound of two plugs with children named as indicated in preceding constant
        :return: `dict` {origin:MPlug, guided:MPlug}
        """
        assert plug.isCompound, "plug is not compound"
        assert plug.numChildren() == 2, "plug has unexpected number of children"
        childrenCount = plug.numChildren()
        trackedPlugsDict = {self.TRACKER_PLUG_NAMES[0]: None, self.TRACKER_PLUG_NAMES[1]: None}
        for j in xrange(childrenCount):
            subPlug = plug.child(j)
            plugKey = subPlug.partialName().rsplit(".", 1)[-1]
            trackedPlugsDict[plugKey] = om2.MPlug().copy(subPlug)

        return trackedPlugsDict

    def active_plugs_from_tracker_dict(self, trackerDict):
        retDict = {self.TRACKER_PLUG_NAMES[0]: None, self.TRACKER_PLUG_NAMES[1]: None, "guidedSource": None}
        assert isinstance(trackerDict, dict)
        for k in self.TRACKER_PLUG_NAMES:
            assert k in trackerDict

        if trackerDict[self.TRACKER_PLUG_NAMES[0]].isDestination:
            retDict[self.TRACKER_PLUG_NAMES[0]] = om2.MPlug().copy(trackerDict[self.TRACKER_PLUG_NAMES[0]].source())

        assert trackerDict[
                   self.TRACKER_PLUG_NAMES[1]] is not None, "received a None guided plug, this should never happen"
        retDict[self.TRACKER_PLUG_NAMES[1]] = om2.MPlug().copy(trackerDict[self.TRACKER_PLUG_NAMES[1]].source())

        guidedPlugSource = om2.MPlug().copy(retDict[self.TRACKER_PLUG_NAMES[1]].source())
        if not guidedPlugSource.isNull:
            retDict["guidedSource"] = guidedPlugSource

        return retDict

    def get_relevant_joint_info(self, plug_outChain, elems_outChain):

        jnts_skin_index =[]

        for i in xrange(elems_outChain):

            plug = plug_outChain.elementByPhysicalIndex(i)
            for eachPlugDest in plug.destinations():

                node = eachPlugDest.node()
                mfn_node = om2.MFnDependencyNode(node)

                if not node.hasFn(om2.MFn.kContainer):

                    if node.hasFn(om2.MFn.kMatrixMult):
                        mfn_mtxMult = om2.MFnDependencyNode(node)

                        plug_mtxMult = mfn_mtxMult.findPlug("matrixSum", False)
                        for eachPlugMtx in plug_mtxMult.destinations():
                            mob_decMtx = eachPlugMtx.node()

                    else:
                        mob_decMtx = node

            if mob_decMtx is None:
                raise RuntimeError("{}: no decompose matrix was assigned to variable mob_decMtx")

            mfn_decMtx = om2.MFnDependencyNode(mob_decMtx)
            plug_decMtx = mfn_decMtx.findPlug("outputTranslate", False)

            for eachPlugDecDest in plug_decMtx.destinations():
                node = eachPlugDecDest.node()

                if node.hasFn(om2.MFn.kJoint):
                    mfn_jnt = om2.MFnDependencyNode(node)

                else:
                    om2.MGlobal.displayError("{} : is not of joint type".format(node))

            plug_jnt = mfn_jnt.findPlug("worldMatrix", False)
            elem_plug_jnt = plug_jnt.evaluateNumElements()

            for eachPlug in xrange(elem_plug_jnt):

                plug_worldMtx = plug_jnt.elementByPhysicalIndex(eachPlug)
                for eachDest in plug_worldMtx.destinations():
                    dest_name = eachDest.name()
                    skinClst_name = dest_name.split(".")[0]
                    attr_name = dest_name.split(".")[1]
                    array_index = attr_name.replace("matrix", "")

                    jnts_skin_index.append((mfn_jnt.name(), skinClst_name, array_index))

        return jnts_skin_index

    def iterate_outchain_to_bind(self, output):
        """This takes a well formed plug that is a compound and has two children and forms a dictionary from it
        containing those two sub plugs indexed by their expected names.
        :param output: MFnDagNode object of an output node of a component.
        :returns list[tuple(str)]: Returns a list of tuples containing joint name, skin cluster and skinClusterArrayIndex in this order.
        """

        jnts_skin_index = []

        plug_exist = self.check_plug_exist(output, "outChainToBind")

        if plug_exist:

            plug_outChain = output.findPlug("outChainToBind", False)
            attr_outChain = plug_outChain.attribute()

            if plug_outChain.isCompound:
                if plug_outChain.isArray:
                    elems_compound_outChain = plug_outChain.evaluateNumElements()
                    for i in range(elems_compound_outChain):
                        plug_sub_outChain = plug_outChain.elementByPhysicalIndex(i)
                        if plug_sub_outChain.isCompound:
                            plug_sub_child = plug_sub_outChain.child(0)
                            elems_sub_child = plug_sub_child.evaluateNumElements()
                            sub_jnts_skin_index = self.get_relevant_joint_info(plug_sub_child, elems_sub_child)
                            for item in sub_jnts_skin_index:
                                jnts_skin_index.append(item)
                        else:
                            raise RuntimeError("{}: is not a compound attribute".format (sub_plug_outChain))
                        
                else:
                    raise RuntimeError(
                        "{}: is a compound attribute but doesnt have any outChainToBindSub child attributes".format(
                            plug_outChain.name()))

            elif attr_outChain.hasFn(om2.MFn.kMatrixAttribute):
                elems_outChain = plug_outChain.evaluateNumElements()
                jnts_skin_index = self.get_relevant_joint_info(plug_outChain, elems_outChain)
            else:
                raise RuntimeError("{}:  is not of compound or matrix type".format(plug_outChain.name()))

            return jnts_skin_index
        else:
            print "{}: Does not have an .outChainToBind plug".format(output.name())
            return None

    def get_component_object(self, componentNameAndMobha, objectType=""):
        """

        :param componentNameAndMobha: `tuple` (`str`, <OpenMaya.MObjectHandle>)
        :param objectType: `str` guide_tool, output, deform, control, guide
        :returns `MFnDagNode`: components object
        """
        mfn = None
        objName = objectType
        compName = componentNameAndMobha[0]
        mobhaContainer = componentNameAndMobha[1]

        keyObs = self.key_objects_from_container(compName, mobhaContainer)
        mobha = keyObs[objName]

        if mobha is not None and mobha.isValid():
            mob = mobha.object()
            if mob.hasFn(om2.MFn.kDagNode):
                mfn = om2.MFnDagNode(mob)
            elif mob.hasFn(om2.MFn.kDependencyNode):
                mfn = om2.MFnDependencyNode(mob)
            return mfn
        elif mobha is None:
            return mfn

    def guide_toggle(self, guide_tool, output):
        """
        Toggle between guided or non guided mode

        Args:
        :param guide_tool: `MFnDagNode` components tool panel node
        :param output: `MFnDagNode` components output node
        :return boolean: True if component is guided
        """
        is_guided = None
        plug_is_affected_exist = self.check_plug_exist(output, "isAffected")

        toSwapPlug = guide_tool.findPlug("toSwap", False)
        elemCount = toSwapPlug.evaluateNumElements()

        plug_is_guided = guide_tool.findPlug("isGuided", False)
        plug_guide_visibility = guide_tool.findPlug("showGuides", False)

        toolPanelCouples = [None] * elemCount
        activePlugCouples = [None] * elemCount

        for i in xrange(elemCount):
            # fetching all guide tools plugs first
            elemPlug = toSwapPlug.elementByPhysicalIndex(i)

            trackerPlugsDict = self.tracker_subplugs_from_plug(elemPlug)
            # todo: replace outer scope doing this job with a map
            toolPanelCouples[i] = trackerPlugsDict

            # going from tracker plugs to active ones
            activePlugsDict = self.active_plugs_from_tracker_dict(trackerPlugsDict)

            doNothing = activePlugsDict["origin"] is None and activePlugsDict["guidedSource"] is None
            disconnect = activePlugsDict["origin"] is None and activePlugsDict["guidedSource"] is not None
            connect = activePlugsDict["origin"] is not None and activePlugsDict["guidedSource"] is None
            swap = activePlugsDict["origin"] is not None and activePlugsDict["guidedSource"] is not None

            if doNothing:
                pass
            elif connect:

                activeOrigin_name = activePlugsDict["origin"].partialName(useFullAttributePath=True,
                                                                          includeNodeName=True,
                                                                          useLongNames=True)
                trackerOrigin_name = trackerPlugsDict["origin"].partialName(useFullAttributePath=True,
                                                                            includeNodeName=True,
                                                                            useLongNames=True)
                activeGuided_name = activePlugsDict["guided"].partialName(useFullAttributePath=True,
                                                                          includeNodeName=True,
                                                                          useLongNames=True)

                cmds.connectAttr(activeOrigin_name, activeGuided_name)
                cmds.disconnectAttr(activeOrigin_name, trackerOrigin_name)
                # Show guides
                if not plug_guide_visibility.asBool():
                    plug_guide_visibility.setBool(True)
                if not plug_is_guided.asBool():
                    plug_is_guided.setBool(True)
                is_guided = True

            elif disconnect:

                activeGuidedSource_name = activePlugsDict["guidedSource"].partialName(useFullAttributePath=True,
                                                                                      includeNodeName=True,
                                                                                      useLongNames=True)
                activeGuided_name = activePlugsDict["guided"].partialName(useFullAttributePath=True,
                                                                          includeNodeName=True,
                                                                          useLongNames=True)
                trackerOrigin_name = trackerPlugsDict["origin"].partialName(useFullAttributePath=True,
                                                                            includeNodeName=True,
                                                                            useLongNames=True)

                cmds.disconnectAttr(activeGuidedSource_name, activeGuided_name)
                cmds.connectAttr(activeGuidedSource_name, trackerOrigin_name)
                # Hide guides
                if plug_is_guided.asBool():
                    plug_guide_visibility.setBool(False)
                if plug_is_guided.asBool():
                    plug_is_guided.setBool(False)

                if plug_is_affected_exist:
                    plug_is_affected = output.findPlug("isAffected", False)

                    if plug_is_affected.asBool():
                        self.affect_skin_bindings(output, guide_tool)
                        plug_is_affected.setBool(False)

                is_guided = False

            elif swap:

                activeOrigin_name = activePlugsDict["origin"].partialName(useFullAttributePath=True,
                                                                          includeNodeName=True,
                                                                          useLongNames=True)
                trackerOrigin_name = trackerPlugsDict["origin"].partialName(useFullAttributePath=True,
                                                                            includeNodeName=True,
                                                                            useLongNames=True)
                activeGuided_name = activePlugsDict["guided"].partialName(useFullAttributePath=True,
                                                                          includeNodeName=True,
                                                                          useLongNames=True)
                activeGuidedSource_name = activePlugsDict["guidedSource"].partialName(useFullAttributePath=True,
                                                                                      includeNodeName=True,
                                                                                      useLongNames=True)
                cmds.connectAttr(activeGuidedSource_name, trackerOrigin_name, force=True)
                cmds.connectAttr(activeOrigin_name, activeGuided_name, force=True)

                if plug_is_guided.asBool():
                    plug_is_guided.setBool(False)
                    if plug_is_affected_exist:
                        plug_is_affected = output.findPlug("isAffected", False)
                        if plug_is_affected.asBool():
                            self.affect_skin_bindings(output, guide_tool)
                            plug_is_affected.setBool(False)
                    is_guided = False
                elif not plug_is_guided.asBool():
                    plug_is_guided.setBool(True)
                    is_guided = True

            else:
                # todo: raise meaningful error with sensible message
                raise RuntimeError("should have never got here!")
                # fetching all the jnts bindings and relevant info

        return is_guided

    def affect_skin_bindings(self, output, guide_tool):
        """
        Makes the guide mode affect the bind joints and let you move them without affecting the skinning.
        Connects the inverse world matrix of each node in the the .outChainToBind plug of the components putput to the
        skincluster
        :param output: `MFnDagNode`
        :param guide_tool: `MFnDagNode`
        :return boolean: True if the component is in guide mode, false if not and none if the plug wasnt found.
        """
        is_affected = None
        plug_is_guided_exist = self.check_plug_exist(guide_tool, "isGuided")
        plug_is_affected_exist = self.check_plug_exist(output, "isAffected")

        if plug_is_guided_exist:

            plug_is_guided = guide_tool.findPlug("isGuided", False)

            jnts_skin_index = self.iterate_outchain_to_bind(output)
            print jnts_skin_index
            if plug_is_guided.asBool():
                if jnts_skin_index is not None:
                    for k in jnts_skin_index:
                        try:
                            cmds.connectAttr("{}.worldInverseMatrix[0]".format(k[0]),
                                             "{0}.bindPreMatrix{1}".format(k[1], k[2]), force=True)
                        except:
                            print "already connected"

                    if plug_is_affected_exist:
                        plug_is_affected = output.findPlug("isAffected", False)
                        plug_is_affected.setBool(True)
                        is_affected = True

            elif not plug_is_guided.asBool():

                for k in jnts_skin_index:
                    try:
                        cmds.disconnectAttr("{}.worldInverseMatrix[0]".format(k[0]),
                                            "{0}.bindPreMatrix{1}".format(k[1], k[2]))

                        inv_mtx = om2.MMatrix(cmds.getAttr("{}.worldInverseMatrix[0]".format(k[0])))
                        cmds.setAttr("{0}.bindPreMatrix{1}".format(k[1], k[2]), inv_mtx, type="matrix")

                    except:
                        print "already disconnected"

                if plug_is_affected_exist:
                    plug_is_affected = output.findPlug("isAffected", False)
                    plug_is_affected.setBool(False)
                    is_affected = False

            else:
                om2.MGlobal.displayError("Something went wrong with the skinConnections or bindPreMatricies")

        else:
            print "{}: Does not have an .isGuided plug".format(guide_tool.name())

        return is_affected

    def get_opposite_component_name(self, name):
        namespaced = False
        # Remove namespace
        if ":" in name:
            name = name.split(":")[-1]
            namespaced = True
        if "_L_" in name:
            name_opposite = name.replace("_L_", "_R_")
        elif "_R_" in name:
            name_opposite = name.replace("_R_", "_L_")
        else:
            name_opposite = None
        return name_opposite, namespaced

    def get_guide_ctrl_objects(self, source_component, target_component, guide_control_suffix="guide_control"):
        """ Mirror the selected component as source, uses the side part of the name and replace to the opposit side

        :param source_component: `str` A container node
        :param target_component: `str` A container node
        :return boolean: True if created succesfully
        """

        component_dict = {
            "source": None,
            "target": None
        }

        components = [source_component, target_component]
        loop_num = 0

        for k, v in component_dict.items():
            mfn_guide = self.get_mfn_from_component(components[loop_num], "guide")[0]

            children_elems = mfn_guide.childCount()
            for i in range(children_elems):
                mob_child = mfn_guide.child(i)

                fullpath_child = om2.MFnDagNode(mob_child).fullPathName()
                if fullpath_child.endswith(guide_control_suffix):
                    v = fullpath_child.split("|")[-1]
                    guides = cmds.listRelatives(v, allDescendents=True, type="transform")
                    component_dict[k] = guides
                else:
                    continue
            loop_num += 1

        return component_dict

    def mirror_guides(self, component_dict):

        target_items = component_dict["target"]
        component_elements = len(component_dict["source"])

        for guide in range(component_elements):

            source_guide = component_dict["source"][guide]

            target_trans_x = cmds.getAttr("{}.translateX".format(source_guide)) * -1
            target_trans_y = cmds.getAttr("{}.translateY".format(source_guide))
            target_trans_z = cmds.getAttr("{}.translateZ".format(source_guide))
            target_trans = [target_trans_x, target_trans_y, target_trans_z]

            target_rot_x = cmds.getAttr("{}.rotateX".format(source_guide)) * -1
            target_rot_y = cmds.getAttr("{}.rotateY".format(source_guide)) * -1
            target_rot_z = cmds.getAttr("{}.rotateZ".format(source_guide)) * -1
            target_rot = [target_rot_x, target_rot_y, target_rot_z]
            axis = ["X", "Y", "Z"]

            for ax in range(len(axis)):
                if not cmds.getAttr("{}.translate{}".format(target_items[guide], axis[ax]), lock=True):
                    cmds.setAttr("{}.translate{}".format(target_items[guide], axis[ax]), target_trans[ax])

                if not cmds.getAttr("{}.rotate{}".format(target_items[guide], axis[ax]), lock=True):
                    cmds.setAttr("{}.rotate{}".format(target_items[guide], axis[ax]), target_rot[ax])

    def delete_dg_nodes(self, guide_node):
        # DG deletion
        # JUST ADDED, DOES IT WORK?
        if guide_node is not None:
            toDeletePlug = guide_node.findPlug("toDelete", False)
            elemCount = toDeletePlug.evaluateNumElements()

            if elemCount is not None:
                for i in xrange(elemCount):
                    elemPlug = toDeletePlug.elementByPhysicalIndex(i)

                    if elemPlug.isDestination:
                        sourceNode = elemPlug.source().node()

                        nodeName = ""
                        if not sourceNode.hasFn(om2.MFn.kDagNode):
                            nodeName = om2.MFnDependencyNode(sourceNode).name()

                        if nodeName:
                            print nodeName
                            cmds.delete(nodeName)
                            print nodeName + ": has been deleted"

    def delete_dag_nodes(self, guide_node):
        """
        Deletes the guide DAG object in it if present
        :param guide_node: `MFnDagNode`
        :return boolean: True if the guide was found and deleted, False if one wasnt  found
        """
        hasBeenDeleted = False

        if guide_node is not None:
            dagPathToGuide = om2.MDagPath.getAPathTo(guide_node.object()).fullPathName()
            cmds.delete(dagPathToGuide)
            hasBeenDeleted = True
            print dagPathToGuide + ": has been deleted"

        return hasBeenDeleted

    def remove_namespace(self, node_name):
        """
        Gets the namespace of the passed node name and then rename all of its members and then deletes the namespace
        :param node_name: `str` A node which is a member of the namespace
        :return boolean: True if a namespace was found and deleted
        """
        namespace_deleted = False

        if ":" in node_name:
            namespace = om2.MNamespace.getNamespaceFromName(node_name)

            mob_array_namespace_members = om2.MNamespace.getNamespaceObjects(namespace)
            array_len = len(mob_array_namespace_members)

            ## NOTE
            for i in xrange(array_len):
                mob_member = mob_array_namespace_members[i]
                print mob_member
                member_name = om2.MFnDependencyNode(mob_member).name()
                namespace_strip = om2.MNamespace.stripNamespaceFromName(member_name)
                try:
                    cmds.rename(member_name, namespace_strip)
                except:
                    break

            om2.MNamespace.removeNamespace(namespace)
            namespace_deleted = True

        return namespace_deleted

    def toggle_black_box(self, container):
        """
        Enables the black box state of the passed container
        :param container: `str` container node name
        :return boolean: True if the black box was enabled
        """
        enabled_black_box = None

        mob_container = self.create_mob_from_node(container)
        mfn_dg_container = om2.MFnDependencyNode(mob_container)
        plug_black_box_exist = self.check_plug_exist(mfn_dg_container, "blackBox")

        if plug_black_box_exist:

            mob_container = self.create_mob_from_node(container)
            mfn_container = om2.MFnDependencyNode(mob_container)
            plug_black_box = mfn_container.findPlug("blackBox", False)
            plug_black_box_value = plug_black_box.asBool()

            if plug_black_box_value:
                plug_black_box.setBool(False)
                enabled_black_box = False

            elif not plug_black_box_value:
                plug_black_box.setBool(True)
                enabled_black_box = True

        return enabled_black_box


"""
    def import_reference(self, node):

        pass


"""