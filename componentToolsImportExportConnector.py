import os
import glob
import re
from maya import cmds

class ComponentToolsImportExportConnector(object):

    def __init__(self, root_dir = None, component_dir_name = None):
        self.component_dir_name = component_dir_name
        self.root_dir = root_dir
        self.root_sub_dirs = os.listdir(self.root_dir)
        self.cmpnts_fullpath = os.path.join(self.root_dir, self.component_dir_name )

    def strip_version(self, file_name):
        match = re.match(r"^(?P<name>[\w_]+_v)\d+(?P<ext>\.\w+)$", file_name)
        if match:
            return "{name}*{ext}".format(name=match.group("name"), ext=match.group("ext"))
        return file_name

    def get_components_sorted(self):
        unique_cmpnt_names = set()
        cmpnts = []  # list of list
        command = "{}/*_v*.*".format(self.cmpnts_fullpath)
        # fined unique componetent names.
        for component in glob.glob(command):
            name_version_stripped = self.strip_version(os.path.basename(component))
            unique_cmpnt_names.add(name_version_stripped)

        # Generate componets list.
        for unique_cmpnt in unique_cmpnt_names:
            _cmpnt_list = []

            for _file in glob.glob("{}/{}".format(self.cmpnts_fullpath, unique_cmpnt)):
                _cmpnt_list.append(os.path.basename(_file))
            cmpnts.append(_cmpnt_list)

        # sort files.
        for cmpnt_list in cmpnts:
            cmpnt_list.sort()

        return cmpnts

    def import_component(self, component_name, reference_state = False):
        import_result = None
        fullpath = os.path.join(self.root_dir, self.component_dir_name, component_name)

        try:
            cmds.file(fullpath, i=not reference_state, parentNamespace=True, reference= reference_state)
            import_result = True
        except:
            import_result = False

        return import_result