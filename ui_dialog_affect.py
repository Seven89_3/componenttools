# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'D:\3D\SCRIPTS\dev\componentTools\ui_dialog_affect.ui'
#
# Created: Sun Jan 26 20:13:05 2020
#      by: pyside2-uic  running on PySide2 2.0.0~alpha0
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_connectAffection(object):
    def setupUi(self, connectAffection):
        connectAffection.setObjectName("connectAffection")
        connectAffection.resize(288, 201)
        self.verticalLayoutWidget = QtWidgets.QWidget(connectAffection)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(10, 10, 271, 187))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.dialog_affect_layout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.dialog_affect_layout.setContentsMargins(0, 0, 0, 0)
        self.dialog_affect_layout.setObjectName("dialog_affect_layout")
        self.dialog_affect_listWidget = QtWidgets.QListWidget(self.verticalLayoutWidget)
        self.dialog_affect_listWidget.setObjectName("dialog_affect_listWidget")
        self.dialog_affect_layout.addWidget(self.dialog_affect_listWidget)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.dialog_affect_layout.addItem(spacerItem)
        self.dialog_affect_lbl = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.dialog_affect_lbl.setObjectName("dialog_affect_lbl")
        self.dialog_affect_layout.addWidget(self.dialog_affect_lbl)
        self.dialog_affect_btnBox = QtWidgets.QDialogButtonBox(self.verticalLayoutWidget)
        self.dialog_affect_btnBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.dialog_affect_btnBox.setObjectName("dialog_affect_btnBox")
        self.dialog_affect_layout.addWidget(self.dialog_affect_btnBox)

        self.retranslateUi(connectAffection)
        QtCore.QMetaObject.connectSlotsByName(connectAffection)

    def retranslateUi(self, connectAffection):
        connectAffection.setWindowTitle(QtWidgets.QApplication.translate("connectAffection", "Connect Affection", None, -1))
        self.dialog_affect_lbl.setText(QtWidgets.QApplication.translate("connectAffection", "Chose the source output which are affecting the target", None, -1))

