from PySide2 import QtCore, QtGui, QtWidgets
from maya.api import OpenMaya as om2
from maya import OpenMayaUI as omui
from maya.app.general import mayaMixin
from shiboken2 import wrapInstance

from componentTools import ui
from componentTools import ui_dialog_affect

reload(ui)
reload(ui_dialog_affect)

from componentTools import componentToolsGuideConnector

reload(componentToolsGuideConnector)
from componentTools import componentToolsCreateConnector

reload(componentToolsCreateConnector)


def getMainWindowPtr():
    mayaMainWindowPtr = omui.MQtUtil.mainWindow()
    mayaMainWindow = wrapInstance(long(mayaMainWindowPtr), QtWidgets.QWidget)
    return mayaMainWindow

class AffectDialog(QtWidgets.QDialog, ui_dialog_affect.Ui_connectAffection):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        self.setupUi(self)

class ComponentWidget(mayaMixin.MayaQWidgetDockableMixin, QtWidgets.QWidget, ui.Ui_componentRigTools):
    def __init__(self, parent=None, connector_guide=None, connector_create=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.connector_create = connector_create
        self.connector_guide = connector_guide
        self.setupUi(self)
        self.populate_ui()
        self.setup_connections()
        self.affect_dialog = AffectDialog()
        self.affect_connections()

    def populate_ui(self):
        self.reload_component()

    def setup_connections(self):
        self.selected_components = self.guide_listWidget.itemSelectionChanged.connect(self.get_selected_components)
        # Create Tab
        self.create_component_btn.clicked.connect(self.create_component)
        self.connect_affect_btn.clicked.connect(self.affect_dialog_open)
        self.binding_btn.clicked.connect(self.binding_button_clicked)

        # Guide Tab
        self.reload_btn.clicked.connect(self.reload_component)
        self.guide_btn.clicked.connect(self.guide_button_clicked)
        self.mirror_btn.clicked.connect(self.mirror_button_clicked)
        self.delete_btn.clicked.connect(self.delete_button_clicked)
        self.finalize_btn.clicked.connect(self.finalize_button_clicked)

    def affect_connections(self):
        self.affect_dialog.dialog_affect_btnBox.rejected.connect(self.affect_dialog.reject)
        self.affect_dialog.dialog_affect_btnBox.accepted.connect(self.affect_dialog_applied)

    def reload_component(self):
        self.guide_listWidget.clear()  # `self.guide_listWidget` name of QListWidget.
        self.guide_listWidget.addItems(self.connector_guide.get_containers_in_scene())
        listWidget_length = self.guide_listWidget.count()

        for i in range(listWidget_length):

            list_item = self.guide_listWidget.item(i)
            container_name = list_item.text()
            mfn_list = self.connector_guide.get_mfn_from_component(container_name, "guide_tool", "output")
            mfn_dag_guitool = mfn_list[0]
            mfn_dag_output = mfn_list[1]

            if not mfn_dag_guitool is None:
                guide_mode = self.connector_guide.check_if_guided(mfn_dag_guitool)
                color_value = self.get_guide_state_color(list_item, guide_mode)
                list_item.setForeground(QtGui.QColor(color_value))

            if not mfn_dag_output is None:
                plug_is_affected_exist = self.connector_guide.check_plug_exist(mfn_dag_output, "isAffected")
                if plug_is_affected_exist:
                    plug_is_affected = mfn_dag_output.findPlug("isAffected", False)
                    if plug_is_affected.asBool():
                        list_item.setForeground(QtGui.QColor("#ff0a0a"))

            else:
                print "{}: Does not have a guide_tool node".format(container_name)

    def create_component(self):
        component_name = self.create_component_text.text()
        component_result = self.connector_create.create_component_hierarchy(component_name)
        if component_result:
            print "{}: Was succesfully created".format(component_name)
        else:
            print "{}: Was not created".format(component_name)

    def affect_dialog_open(self):
        self.affect_dialog.dialog_affect_listWidget.clear()
        self.affect_dialog.show()

        selection_list = self.connector_create.mob_from_active_selection()

        if not len(selection_list) == 2:
            print "bajskorv"
        else:
            mob_source_container = self.connector_guide.container_from_node(selection_list[0])
            name_source_container = self.connector_guide.mob_to_name(mob_source_container)
            mob_target_container = self.connector_guide.container_from_node(selection_list[1])
            self.name_target_container = self.connector_guide.mob_to_name(mob_target_container)

            mfn_source_output = self.connector_guide.get_mfn_from_component(name_source_container, "output")[0]

            output_outchain = self.connector_create.get_out_chain_elements(mfn_source_output, string=True)
            self.affect_dialog.dialog_affect_listWidget.addItems(output_outchain)

    def affect_dialog_applied(self):
        selected_plug = self.affect_dialog.dialog_affect_listWidget.selectedItems()[0]
        selected_plug = selected_plug.text()

        self.connector_create.connect_affection(selected_plug, self.name_target_container)

    def get_guide_state_color(self, item, guide_mode):
        """
        This function ta
        :param item: `QWidgetItem` from the desired component
        :param guide_mode: `bool` guided = True, not guided = False
        :return: `list` MFnDagNodes
        """
        color_dict = {
            "red": "#ff80a1",
            "green": "#75d175",
            "blue": "#414df2",
        }

        if guide_mode:
            color_value = color_dict["red"]
        elif not guide_mode:
            color_value = color_dict["green"]

        return color_value

    def get_selected_components(self):
        """
        Get the selected componets from the list widget
        :return: `list` strings
        """
        selected_components = []

        for item in self.guide_listWidget.selectedItems():
            selected_components.append(item.text())

        return selected_components

    def binding_button_clicked(self):
        component_list = []
        listwidget_length = self.guide_listWidget.count()

        for i in range(listwidget_length):
            list_item = self.guide_listWidget.item(i)
            component_list.append(list_item.text())

        mobha_top_component = self.connector_guide.get_top_component(component_list)

        self.connector_create.build_binding(mobha_top_component)

    def affect_skin_binding(self, component, mfn_guitool, mfn_output):

        is_guided = self.connector_guide.guide_toggle(mfn_guitool, mfn_output)
        color_value = self.get_guide_state_color(component, is_guided)
        component.setForeground(QtGui.QColor(color_value))

        if self.guide_cb.isChecked():
            is_affected = self.connector_guide.affect_skin_bindings(mfn_output, mfn_guitool)
            if is_affected:
                component.setForeground(QtGui.QColor("#ff0a0a"))
            if not is_affected:
                component.setForeground(QtGui.QColor(color_value))
            if is_affected is None:
                pass

    def guide_button_clicked(self):

        selected_components = self.guide_listWidget.selectedItems()

        for component in selected_components:
            name = component.text()

            mfn_list = self.connector_guide.get_mfn_from_component(name, "guide_tool", "output")
            mfn_guitool = mfn_list[0]
            mfn_output = mfn_list[1]

            # Add mfn_output as argument, will disconnect the affected binding if they are effected regardless
            self.affect_skin_binding(component, mfn_guitool, mfn_output)

            self.guide_listWidget.setItemSelected(component, False)

    def mirror_button_clicked(self):
        component = self.guide_listWidget.selectedItems()
        # Make sure only on is selected, for preventing cross mirroring
        name_source = component[0].text()
        name_target, namespaced = self.connector_guide.get_opposite_component_name(name_source)

        if not name_target == None:

            listwidget_length = self.guide_listWidget.count()

            if namespaced:
                for i in range(listwidget_length):
                    list_item = self.guide_listWidget.item(i).text()
                    if name_target in list_item:
                        name_target = list_item

            mfn_output, mfn_guide, mfn_guitool = self.connector_guide.get_mfn_from_component(name_target, "output",
                                                                                             "guide", "guide_tool")
            component = self.guide_listWidget.findItems(name_target, QtCore.Qt.MatchExactly)[0]
            self.affect_skin_binding(component, mfn_guitool, mfn_output)

            component_dict = self.connector_guide.get_guide_ctrl_objects(name_source, name_target)

            self.connector_guide.mirror_guides(component_dict)

        else:
            raise RuntimeError("{}: Is not possible to mirror".format(name_source))

        self.guide_listWidget.setItemSelected(component, False)

    def delete_button_clicked(self):

        selected_components = self.guide_listWidget.selectedItems()

        for component in selected_components:
            name = component.text()

            if self.deleteDg_cb.isChecked():
                mfn_dag_guitool = self.connector_guide.get_mfn_from_component(name, "guide_tool")[0]
                if mfn_dag_guitool is None:
                    raise RuntimeError ("{}: Does not have a guide_tool node present".format(name))
                else:
                    component_dict = self.connector_guide.delete_dg_nodes(mfn_dag_guitool)

            if self.deleteDag_cb.isChecked():
                mfn_dag_guide = self.connector_guide.get_mfn_from_component(name, "guide")[0]
                self.connector_guide.delete_dag_nodes(mfn_dag_guide)

    def finalize_button_clicked(self):

        selected_components = self.guide_listWidget.selectedItems()

        for component in selected_components:
            name = component.text()

            if self.finalizeNamespace_cb.isChecked():
                namespace_deleted = self.connector_guide.remove_namespace(name)
                if namespace_deleted:
                    print "{}: has been deleted succesfully ".format(name)
            if self.finalizeImport_cb.isChecked():
                pass
            if self.finalizeBlackBox_cb.isChecked():
                print name
                black_box_state = self.connector_guide.toggle_black_box(name)
                if black_box_state:
                    print "{}: black box enabled succesfully".format(name)
                if not black_box_state:
                    print "{}: black box disabled succesfully".format(name)

try:
    window.close()
except:
    pass

window = ComponentWidget(connector_guide=componentToolsGuideConnector.ComponentToolsGuideConnector(),
                         connector_create=componentToolsCreateConnector.ComponentToolsCreateConnector())
window.show()
# window.show(dockable=True, area="right", floating=False)
